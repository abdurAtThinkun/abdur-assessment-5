﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Net.Wifi;
using System;

namespace ToggleWifi
{
    [Activity(Label = "ToggleWifi", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
      

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button receiverButton = FindViewById<Button>(Resource.Id.myButton);

            receiverButton.Click += (sender, e) => GoToWifiSettings();
        }

        public void GoToWifiSettings()
        {
            var toWifiSettings = new Intent(Android.Provider.Settings.ActionWifiSettings);
            StartActivity(toWifiSettings);
        }


    }
}

