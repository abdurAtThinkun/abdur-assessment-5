﻿using System;
using Android.App;
using Android.Content;
using Android.Net;
using Android.Widget;

namespace ToggleWifi
{
    [BroadcastReceiver()]
    [IntentFilter(new[] { Android.Net.ConnectivityManager.ConnectivityAction, Android.Content.Intent.ActionBootCompleted })]
    public class NetworkStatusBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            
            Toast.MakeText(context, "The network has changed", ToastLength.Long).Show();
        }


    }
}